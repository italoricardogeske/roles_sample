<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $user = Auth::guard()->user();

        foreach ($roles as $role) {
            if ($user->hasAuthorization($role)) {
                return $next($request);
            }
        }
        throw new AuthorizationException('You do not have permission to view this page');
    }
}
