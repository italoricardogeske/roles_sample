<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\shop;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShopPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any shops.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->userRoles->contains(function ($userRole, $key) {
            return $userRole->role->role == Role::ROLE_ADMIN || $userRole->role->role == Role::ROLE_MOD || $userRole->role->role == Role::ROLE_VIEWER;
        });
    }

    /**
     * Determine whether the user can view the shop.
     *
     * @param  \App\User  $user
     * @param  \App\shop  $shop
     * @return mixed
     */
    public function view(User $user, shop $shop)
    {
        if ($this->isAuthorized($user)) {
            return true;
        }
        return $user->shop_id == $shop->id;
    }

    /**
     * Determine whether the user can create shops.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isAuthorized($user);
    }

    /**
     * Determine whether the user can update the shop.
     *
     * @param  \App\User  $user
     * @param  \App\shop  $shop
     * @return mixed
     */
    public function update(User $user, shop $shop)
    {
        if ($this->isAuthorized($user)) {
            return true;
        }
        return $user->shop_id == $shop->id;
    }

    /**
     * Determine whether the user can delete the shop.
     *
     * @param  \App\User  $user
     * @param  \App\shop  $shop
     * @return mixed
     */
    public function delete(User $user, shop $shop)
    {
        if ($this->isAuthorized($user)) {
            return true;
        }
        return $user->shop_id == $shop->id;
    }

    /**
     * Determine whether the user can restore the shop.
     *
     * @param  \App\User  $user
     * @param  \App\shop  $shop
     * @return mixed
     */
    public function restore(User $user, shop $shop)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the shop.
     *
     * @param  \App\User  $user
     * @param  \App\shop  $shop
     * @return mixed
     */
    public function forceDelete(User $user, shop $shop)
    {
        //
    }

    private function isAuthorized($user) {
        return $user->hasAuthorization(Role::ROLE_MOD);
    }
}
