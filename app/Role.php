<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ROLE_ADMIN = 1;
    const ROLE_MOD = 2;
    const ROLE_VIEWER = 3;

    protected $guarded = [];

    public function userRoles() {
        return $this->hasMany(UserRole::class);
    }
}
