<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'roles' => 'array'
    ];

    public function shop() {
        return $this->belongsTo(Shop::class);
    }

    public function userRoles() {
        return $this->hasMany(UserRole::class);
    }

    public function hasAuthorization($role) {
        return $this->userRoles->contains(function ($userRole, $key) use ($role) {
            return $userRole->role->role <= $role;
        });
    }
}
