<?php

use Illuminate\Database\Seeder;
use App\Shop;

class ShopSeeder extends Seeder
{
    protected $sampleShops = [
        'hakataten', 'maizuruten', 'sumiyoshiten', 'ogooriten'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->sampleShops as $sampleShop) {
            Shop::create(['name' => $sampleShop]);
        }
    }
}
