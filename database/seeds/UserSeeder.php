<?php

use Illuminate\Database\Seeder;
use App\Shop;
use App\Role;
//use Hash;

class UserSeeder extends Seeder
{
    protected $roles;
    protected $sampleUsers = [
        ['name' => 'admin', 'email' => 'admin@test', 'password' => '12345678'],
        ['name' => 'mod', 'email' => 'mod@test', 'password' => '12345678'],
        ['name' => 'dummy1', 'email' => 'dummy1@test', 'password' => '12345678'],
        ['name' => 'dummy2', 'email' => 'dummy2@test', 'password' => '12345678'],
        ['name' => 'dummy3', 'email' => 'dummy3@test', 'password' => '12345678']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->roles = Role::all();

        foreach (Shop::all() as $key => $shop) {
            foreach ($this->sampleUsers as $sampleUser) {
                $user = $this->createUser($shop, $sampleUser, $key);
                if ($user->name == 'admin') {
                    $this->addRoleToUser( Role::ROLE_ADMIN, $user);
                } elseif ($user->name == 'mod') {
                    $this->addRoleToUser( Role::ROLE_MOD, $user);
                }
            }
        }
    }

    private function createUser($shop, $sampleUser, $index) {
        return  $shop->users()->create([
            'name' => $sampleUser['name'],
            'email' => $sampleUser['email'].$index,
            'password' => Hash::make($sampleUser['password'])
        ]);
    }

    private function addRoleToUser(int $role, $user) {
        $role = $this->roles->firstWhere('role', $role);
        $user->userRoles()->create(['role_id' => $role->id]);
    }
}
