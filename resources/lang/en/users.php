<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'list' => 'Users List',
    'name' => 'Name',
    'roles' => 'Roles',
    'created_at' => 'Created At',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'shop' => 'Shop'
];
