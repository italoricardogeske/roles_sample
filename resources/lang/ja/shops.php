<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'list' => '店舗一覧',
    'name' => '店名',
    'created_at' => '作成日',
    'delete' => '削除',
    'edit' => '編集'

];
