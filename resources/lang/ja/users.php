<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'list' => 'ユーザー一覧',
    'name' => 'ユーザー名',
    'roles' => '所有権',
    'created_at' => '登録日',
    'delete' => '削除',
    'edit' => '編集',
    'shop' => '店舗'

];
