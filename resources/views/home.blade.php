@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        @can('see', \App\Role::ROLE_ADMIN)
                            <a href="{{ route('users.index')  }}" class="list-group-item list-group-item-action">{{ __('users.list') }}</a>
                        @endcan
                        @can('see',\App\Role::ROLE_MOD)
                            <a href="{{ route('roles.index')  }}" class="list-group-item list-group-item-action">{{ __('roles.list') }}</a>
                        @endcan
                        <a href="{{ route('shops.index')  }}" class="list-group-item list-group-item-action">{{ __('shops.list') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
