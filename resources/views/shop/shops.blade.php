@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('shops.list')  }}</div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('shops.name')  }}</th>
                                <th scope="col">{{ __('shops.created_at')  }}</th>
                                <th style="width: 20%" scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shops as $shop)
                                <tr>
                                    <th scope="row">{{ $shop->id }}</th>
                                    <td>{{ $shop->name  }}</td>
                                    <td>{{ $shop->created_at  }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary">{{ __('shops.edit')  }}</button>
                                        <button type="button" class="btn btn-danger">{{ __('shops.delete')  }}</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
