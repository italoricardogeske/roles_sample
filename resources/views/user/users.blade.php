@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('users.list')  }}</div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('users.name')  }}</th>
                                <th scope="col">{{ __('users.roles')  }}</th>
                                <th scope="col">{{ __('users.shop')  }}</th>
                                <th scope="col">{{ __('users.created_at')  }}</th>
                                <th style="width: 20%" scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td>{{ $user->name  }}</td>
                                    <td>{{ implode($user->roles, ',') }}</td>
                                    <td>{{ $user->shop->name  }}</td>
                                    <td>{{ $user->created_at  }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary">{{ __('users.edit')  }}</button>
                                        <button type="button" class="btn btn-danger">{{ __('users.delete')  }}</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
